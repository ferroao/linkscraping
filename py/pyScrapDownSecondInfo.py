# reticulate::repl_python() # R

#install inside python
# import subprocess
# subprocess.call(['pip3', 'install', "pandas"])

import time
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import pandas as pd

def mainloopComple(start,end,monthStart,monthEnd,csvname):
    options = Options()
    options.add_argument('--headless')
    d = []
    for year in range(start, end+1, 1):
        year = str(year)
        print(year)
        filename = ''.join(["sourcelinks/", year, ".txt"])
        file = open(filename, 'r')
        url_list = file.readlines()
        # Negative values also work to make a copy of the same list in reverse order:
        url_list = url_list[::-1]
        # for url in url_list:
        for url in url_list[monthStart-1:monthEnd]:
            print(url)
            # driver = webdriver.Chrome()
            driver = webdriver.Chrome(options=options)
            driver.get(url)
            delay = 10 # seconds
            while True:
                try:
                    WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'iyc-views-IssueArticleEntry')))
                    break # it will break from the loop once the specific element will be present. 
                except TimeoutException:
                    print("Loading took too much time!-Try again")
            issue = driver.find_elements_by_class_name("issueNumberInfo")
            issue2 = issue[0].text
            print(issue2)
            secElems = driver.find_elements_by_class_name("sectionName") 
            ulAfterSec = driver.find_elements(By.XPATH, '//p[@class="sectionName"]/following-sibling::ul')
            # len(ulAfterSec)
            appendComple(secElems,ulAfterSec,d,issue2,year)
            driver.close()
    mydf = pd.DataFrame(d)
    mydf['type']= "Small note"
    mydf = mydf.explode("title")
    newname = ''.join(["csv/",csvname,".csv"])
    mydf.to_csv(newname, index =False)
            
            
def appendComple(secElems,ulAfterSec,d,issue2,year):        
    for i in range(0, (len(secElems)), 1):
        splStr = ulAfterSec[i].text.split('\n')
        d.append(
            {
                'year': year,
                'issue': issue2,
                'section': secElems[i].text,
                'title': splStr,
            }
            )
    return(d)
                    

mainloopComple(2020,2020,9,10,"2020_0910_Comple")



