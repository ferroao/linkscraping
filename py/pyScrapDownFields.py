#  pip3 install ffmpeg-normalize FUERA DE PYTHON SE INSTALAN LOS PAQUETES, O SEA E LA TERMINAL DEL MAC- TERMINAL!! (DENTRO >>> noo!!)
# reticulate::repl_python() # R

#install inside python
import subprocess
# subprocess.call(['pip3', 'install', "pandas"])

import time

from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
#from selenium.webdriver.chrome.options import Options

# from bs4 import BeautifulSoup
# import pickle
# import os
# import re
import pandas as pd

# options = webdriver.ChromeOptions() 
# options.add_argument("start-maximized")
    
    # options = Options()
    # options.add_argument('--headless')

# options = Options()
# 
# options.add_argument('--headless')
# 
# options.add_argument('--disable-gpu')
# year = 1984

def mainloopArticles(start,end,monthStart,monthEnd,csvname):
    options = Options()
    options.add_argument('--headless')
    d = []
    for year in range(start, end+1, 1):
        year = str(year)
        print(year)
        filename = ''.join(["sourcelinks/", year, ".txt"])
        file = open(filename, 'r')
        url_list = file.readlines()
        url_list = url_list[::-1]
        print(url_list)
        for url in url_list[monthStart-1:monthEnd]:
            print(url)
            driver = webdriver.Chrome(options=options)
            driver.get(url)
            delay = 10 # seconds
            while True:
                try:
                    WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'iyc-views-IssueArticleEntry')))
                    break # it will break from the loop once the specific element will be present. 
                except TimeoutException:
                    print("Loading took too much time!-Try again")
            issue = driver.find_elements_by_class_name("issueNumberInfo")
            issue2 = issue[0].text
            articles = driver.find_elements_by_class_name("iyc-views-IssueArticleEntry")
            d = articleIterator(articles,d,driver,issue2,year)
            driver.close()
    mydf = pd.DataFrame(d)
    newname = ''.join(["csv/",csvname,".csv"])
    mydf.to_csv(newname, index =False)
            
def articleIterator(articles,d,driver,issue2,year):
    for elem in articles: #driver.find_elements_by_class_name("iyc-views-IssueArticleEntry"): #soup.find_all('div', "iyc-views-IssueArticleEntry"):
        try:
            disc = WebDriverWait(driver, .1).until(lambda d:elem.find_element_by_class_name('discipline'))
            discText = disc.text
        except TimeoutException:
            discText = ""
        sube = elem.find_element_by_class_name("title")
        authText = []
        auth = elem.find_elements_by_class_name("authoring")
        auth = auth[1:]
        for auth2 in auth:
            authText.append(auth2.text)
        authText = ', '.join(authText)
        abs = elem.find_element_by_class_name("abstract")
        d.append(
            {
                'year': year,
                'issue': issue2,
                'area': discText,
                'title': sube.text,
                'author': authText,
                'abstract':  abs.text
            }
            )
    return(d)   

mainloopArticles(2020,2020,monthStart=9,monthEnd=10,csvname="2020_SepOct")



    
