#  pip3 install ffmpeg-normalize FUERA DE PYTHON SE INSTALAN LOS PAQUETES, O SEA E LA TERMINAL DEL MAC- TERMINAL!! (DENTRO >>> noo!!)
# reticulate::repl_python() # R

import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import pickle
import os
import re

def tiny_file_rename(newname, folder_of_download, time_to_wait=30):
    time_counter = 0
    filename = max([f for f in next(os.walk(folder_of_download))[2] ], key=lambda xa :   os.path.getctime(os.path.join(folder_of_download,xa)))
    # print(filename,"b")
    while '.crdownload' in filename:
        time.sleep(1)
        time_counter += 1
        if time_counter > time_to_wait:
            raise Exception('Waited too long for file to download')
    filename = max([f for f in next(os.walk(folder_of_download))[2] ], key=lambda xa :   os.path.getctime(os.path.join(folder_of_download,xa)))
    # print(filename,"e")
    os.rename(os.path.join(folder_of_download, filename), os.path.join(folder_of_download, newname))

#options = webdriver.ChromeOptions() 
#options.add_argument("start-maximized")

def downloadPDF(start,end,monthStart,monthEnd,email,mypassword):
    options = Options()
    options.add_argument('--headless')
    #options = webdriver.ChromeOptions() 
    #options.add_argument("start-maximized")
    for year in range(start, end+1, 1):    
    #for year in range(2020, 2021, 1):
        print(year, end=" ", flush=True)
        year = str(year)
        filename = ''.join(["sourcelinks/",year,".txt"]) #join es parecida a la funcion paste de R
        file1 = open(filename, 'r') 
        Lines = file1.readlines()
        # Negative values also work to make a copy of the same list in reverse order:
        Lines = Lines[::-1]
        # for url in url_list:
        i = 0
        driver = webdriver.Chrome(options=options)    
        for url in Lines[monthStart-1:monthEnd]:
            i += 1
            print(url, end=" ", flush=True)
            driver.get(url) #abre la URL en el navegador
            delay = 10 # seconds
            while True:
                try:
                    WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'iyc-views-IssueArticleEntry')))
                    break # it will break from the loop once the specific element will be present. 
                except TimeoutException:
                    print("Loading took too much time!-Try again")
            if i < 2:
                elem = driver.find_element_by_class_name("popupLogin") #buscaar el elemento de la pagina a partir del tag
                elem.click();
                time.sleep(3)
                # driver.switch_to_window(window_name)
                driver.switch_to.frame("loginFrame")
                time.sleep(3)
                username = driver.find_element_by_class_name("fieldControl") #buscaar el elemento de la pagina a partir del tag
                username.send_keys(email)
                time.sleep(3)
                password = driver.find_element_by_name("Authentication-password") #buscaar el elemento de la pagina a partir del tag
                password.send_keys(mypassword)
                time.sleep(3)
                box = driver.find_element_by_name("Authentication-persistent") #buscaar el elemento de la pagina a partir del tag
                box.click()
                time.sleep(3)
                submit = driver.find_element_by_name("login") #buscaar el elemento de la pagina a partir del tag
                submit.click()
                time.sleep(3)
                elem = driver.find_element_by_class_name("cookiesDisclaimerContent")
                element = elem.find_element_by_tag_name("button")
                driver.execute_script("arguments[0].click();", element)
                time.sleep(1.5)
            pdfdown = driver.find_element_by_class_name("iyc-views-IssueDownloadLink") #buscaar el elemento de la pagina a partir del tag
            linkelem = pdfdown.find_element_by_tag_name("a")
            issue = linkelem.get_attribute('data-tracking-title')
            issue = re.sub(r"[^a-zA-Z0-9\s]+", "", issue)
            issue = re.sub(r"IyC  ", "", issue)
            issue = re.sub(r"\s", "_", issue)
            newname = ''.join([issue,".pdf"])
            # newname = ''.join([year,"_",issue,".pdf"])
            driver.execute_script("arguments[0].click();", linkelem)
            #element.click()
            time.sleep(3)
            x1=0
            while x1==0:
                count=0
                li = next(os.walk(os.getcwd()))[2]
                for x1 in li:
                    if x1.endswith(".crdownload"):
                         count = count+1        
                if count==0:
                    x1=1
                else:
                    x1=0
            time.sleep(3)        
            tiny_file_rename(newname,os.getcwd())
        driver.quit()
        

downloadPDF(2020,2020,8,8)

#tiny_file_rename("myname",os.getcwd())

