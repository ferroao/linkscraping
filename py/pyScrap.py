import time
    
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup

# auto wait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
    
# options.add_argument('--disable-gpu')

def mainloop(start,end,months):
    options = Options()
    options.add_argument('--headless')
    driver = webdriver.Chrome(options=options)
    url = 'https://www.investigacionyciencia.es/revistas/investigacion-y-ciencia/numeros?year='
    for year in range(start, end+1, 1):
        year = str(year)
        url2 = ''.join([url, year])
        
        driver.get(url2) #abre la URL en el navegador
        
        delay = 10 # seconds
        while True:
            try:
                WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'coverColumn')))
                break # it will break from the loop once the specific element will be present. 
            except TimeoutException:
                print("Loading page took too much time!-Try again")
        
        elem = driver.find_element_by_tag_name("body") #buscaar el elemento de la pagina a partir del tag
        
        no_of_covers = 1
        
        while no_of_covers < months:
            elem.send_keys(Keys.PAGE_DOWN)#element es el mismo body d ela linea 30
            covers = driver.find_elements_by_class_name("coverColumn") #buscaar el elemento de la pagina a partir del tag
            no_of_covers = len(covers)
            # print(no_of_covers)
    
        infoColumns = driver.find_elements_by_class_name("infoColumn")
        
        filename = ''.join(["sourcelinks/",year,".txt"]) #join es parecida a la funcion paste de R
        
        linkIterator(filename,infoColumns)
    driver.quit()

def linkIterator(filename,infoColumns):
    myfile = open(filename, 'w')#se abre un archivo para llenarlo
    for info in infoColumns:
        link = info.find_element(By.XPATH, './/*[@class="dateNumber"]/following-sibling::a')
        link = link.get_attribute("href")
        # print(link)
        myfile.write("%s\n" % link) 
    myfile.close() 

mainloop(1980,2019,12)

mainloop(2021,2021,2)

    
    
    
