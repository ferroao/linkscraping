processSectionDT<-function(sections){
sections$CATEGORY<-"letter"
colnames(sections)[which(names(sections)=="section")]<-"keywords"
sections$volume<-sub("^([[:alpha:]]+).*","\\1",sections$issue)
sections$number <- sub(".*?([[:digit:]]+)$","\\1",sections$issue)
sections$pages<-"1--2"
sections$BIBTEXKEY<-paste0("note_",sections$volume,sections$year)
sections$BIBTEXKEY<-make.unique(sections$BIBTEXKEY)
sections$journal<-"Investigación y Ciencia (Scientific American)"
sections<- sections[,setdiff(colnames(sections),c("issue","type") ), with=FALSE]
menhome<-"/home/fernando/.local/share/data/Mendeley Ltd./Mendeley Desktop/"
# dfAll$file<-
sections$decade<-as.character(NA)
sections[grep(198,sections$year),]$decade<-"80s"
sections[grep(199,sections$year),]$decade<-"90s"
sections[grep(200,sections$year),]$decade<-"2000s"
sections[grep(201,sections$year),]$decade<-"2010s"
sections[grep(202,sections$year),]$decade<-"2020s"
sections$file <- with(sections, paste0(menhome,"InveCien/pdfs/",decade,"/",year,"/","N_",number,"__", tools::toTitleCase(tolower(volume) ),"_",year,".pdf" ) )
return(sections)
}

