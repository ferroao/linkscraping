
############################################################################
#
#          for loop - give each entry a page number (if possible)
#
############################################################################

#
#   recién bajado pdf CREATE INDEXFILE
#
# for (folder in decadeF ){
pdfFiles<- list.files(getwd(), full.names=T, pattern="*.pdf")  
pdfFiles

for (file in pdfFiles){
    # print(file)
    newFile<-gsub("N_","IndexP_N_",file)
    newFile<-gsub(".pdf$",".txt",newFile)
    system(paste0("pdftotext -f 2 -l 7 '",file,"' '",newFile,"'" ) )
    system(paste0("sed -i -r -e '/^\\s*?$/d' '",newFile,"'")) 
    system(paste0("LC_ALL=es_ES.UTF-8 sed -i 's/[^[:blank:][:print:]]//g' '",newFile,"'") ) 
    
    # remove special space \u00AD
    system(paste0("sed -i 's/['\"$(python -c 'print u\"\u00AD\".encode(\"utf8\")')\"']//g' '",newFile,"'"))
    # remove leading spaces - trailing sp
    system(paste0("sed -i 's/^ *//g' '",newFile,"'") ) 
    system(paste0("sed -i 's/ *$//g' '",newFile,"'") )
    # merge line not beginning in number
    system(paste0("sed -i -e \':a; N; $!b a; s/\\n\\([A-Za-z¿ ]\\)/ \\1/g\' '",newFile,"'"))
}


# MOVE PDF AND INDEXFILES
system("ls *.txt")
system("ls *.pdf")
# dir.create("pdfs/2020s/2021")
system("mv *.pdf pdfs/2020s/2021")
# dir.create("pdfs/2020s/2021/indexpages")
system("mv *.txt pdfs/2020s/2021/indexpages")

#
#       filename
#

monthArticles <- artsec01022021

{
    start<-1
    i<-start-1
    end<-length(monthArticles$TITLE)
    error<-0
}

# ln -s /home/fernando/GoogleDrive/gitlab/MenCer/pdfsMCer /home/fernando/.local/share/data/Mendeley\ Ltd./Mendeley\ Desktop/MenCer/pdfsMCer

require(stringi)
#
#  function to assign number
#

testInit<-function(initpage,monthArticles,failure){
    initpage <- unique(initpage)
    initpage <- initpage[!is.na(initpage)]
    initpage <- initpage[!initpage>500]
    if(length(initpage)>1){
        initpage <- initpage[!initpage<3]
        initpage <- initpage[order(initpage, decreasing = T)]
    }
    maxLen <- tryCatch(ifelse(length(initpage)>4,4,length(initpage)) ,error=function(e){1} )
    initpage <- initpage[1:maxLen]
    
    if (length(initpage)>1) {
        initpage <- paste0(initpage, collapse = ", ")
    } 
    
    if(length(initpage) ) {
        if(!is.na(initpage)){
            monthArticles$PAGES[i]<<-initpage
            failure<<-FALSE
        }
    } 
}

# sudo apt install tre-agrep

# View(monthArticles)
# monthArticles$PAGES<-"1--5"
# monthArticles$INDEXFILE
# articleTitle<-monthArticles$TITLE[1]
for (articleTitle in monthArticles$TITLE[start:end]){
    # articleTitle<- "¿Quién llama?"
    i <- i+1
    failure<-TRUE
    # #print(articleTitle)
    wc <- sapply(strsplit(articleTitle, "\\s+"), length)
    articleTitleOrig<-articleTitle
    
    articleTitleMod<-sub(" \\([[:alnum:]]+\\)$","", articleTitleOrig)
    # articleTitleMod<-sub("(.*): \\¿.*","\\1",articleTitleMod) # delete everything after interro
    # articleTitleMod<-gsub("\\¿","\\\\¿",articleTitleMod)
    articleTitleMod<-sub("^¿","",articleTitleMod)
    articleTitleMod<-sub("\\?$","",articleTitleMod)
    articleTitleMod<-sub("^«","",articleTitleMod)
    articleTitleMod<-sub("»$","",articleTitleMod)
    
    articleTitleMod<-sub("\u00AD","",articleTitleMod)
    # articleTitle<-sub(" \\([[:alnum:]]+\\)$","", articleTitle)
    # articleTitle<-sub("(.*)\\s\\([I]+\\):.*","\\1",articleTitle)
    
    initpage<-NA
    row <- suppressWarnings(system(paste0("grep -i '", articleTitleMod,"' '",monthArticles$INDEXFILE[i],"'") , intern=T) )
    remove(initpage) #<-NULL
    initpage<- suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
    
    if(length(initpage)) {
        testInit(initpage,monthArticles,failure)
    } # 1st if 
    if (failure) {
        # articleTitleMod<-"some \"some\""
        # articleTitle <-monthArticles[which(monthArticles$PAGES=="1--5"),]$TITLE[1]
        #
        #   quote removal
        #
        articleTitle <- gsub('"','',articleTitleMod)
        
        initpage<-NA
        searchIn<-monthArticles$INDEXFILE[i]
        # searchIn<-monthArticles[which(monthArticles$PAGES=="1--5"),]$INDEXFILE[1]
        
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",searchIn,"'") , intern=T )
                 , warning=function(w){
                 }
        )
        # row<-grep("^[0-9]+.*", row)
        remove(initpage)
        
        initpage<- suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        }
    } # failure
    
    if (failure) {
        #
        #   word variation
        #
        articleTitle <- gsub("conciencia","consciencia",articleTitleMod)
        initpage<-NA
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",monthArticles$INDEXFILE[i],"'") , intern=T )
                 , warning=function(w){
                 }
        )
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        }
    } # failure
    
    
    if (failure) {
        #
        #   remove accents
        #
        articleTitle <- stri_trans_general(str = articleTitleOrig, id = "Latin-ASCII")
        initpage<-NA
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",monthArticles$INDEXFILE[i],"'") , intern=T )
                 , warning=function(w){
                 }
        )
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        }
    } # failure
    
    if (failure) {
        #
        # use caps
        #
        articleTitle <- toupper(articleTitleOrig)
        initpage<-NA
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",monthArticles$INDEXFILE[i],"'") , intern=T )
                 , warning=function(w){
                 }
        )
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        } 
    } # failure
    if (failure) {
        #
        #   substitute quotes
        #
        articleTitle <- gsub('\"([[:alpha:]])',"«\\1",articleTitleOrig)
        articleTitle <- gsub('([[:alpha:]])\"',"\\1»",articleTitle)
        initpage<-NA
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",monthArticles$INDEXFILE[i],"'") , intern=T )
                 , warning=function(w){
                 }
        )
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        } 
    } # failure
    # if (failure &  sapply(strsplit(articleTitleOrig, "\\s+"), length) > 1) {
    if (failure) {#  &  sapply(strsplit(articleTitleOrig, "\\s+"), length) > 1) {
        
        #
        # inter and symb
        #
        articleTitleMod<-sub("¿","",articleTitleMod)
        
        symbols <- c(
            acute = "áéíóúÁÉÍÓÚýÝ",
            grave = "àèìòùÀÈÌÒÙ",
            circunflex = "âêîôûÂÊÎÔÛ",
            tilde = "ãõÃÕñÑ",
            umlaut = "äëïöüÄËÏÖÜÿ",
            cedil = "çÇ"
        )
        #remove special
        articleTitle<-gsub(paste0(unlist(strsplit(toString(paste0(symbols, collapse="") ),"") ),collapse = "|"),"",articleTitleMod)
        
        initpage<-NA
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",monthArticles$INDEXFILE[i],"'") , intern=T )
                 , warning=function(w){
                 }
        )
        
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        }
        
    } # failure
    
    if (failure){# & sapply(strsplit(articleTitleMod, "\\s+"), length) > 1) {
        # articleTitleMod<-monthArticles[which(monthArticles$PAGES=="1--5"),]$TITLE[1]
        
        symbols <- c(
            acute = "áéíóúÁÉÍÓÚýÝ",
            grave = "àèìòùÀÈÌÒÙ",
            circunflex = "âêîôûÂÊÎÔÛ",
            tilde = "ãõÃÕñÑ",
            umlaut = "äëïöüÄËÏÖÜÿ",
            cedil = "çÇ"
        )
        #remove special
        articleTitle<-gsub(paste0(unlist(strsplit(toString(paste0(symbols, collapse="") ),"") ),collapse = "|"),"",articleTitleMod)
        
        initpage<-NA
        searchIn<-monthArticles$INDEXFILE[i]
        searchIn<-monthArticles[which(monthArticles$PAGES=="1--5"),]$INDEXFILE[1]
        
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",searchIn,"'") , intern=T )
                 , warning=function(w){
                 }
        )
        
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        }
        
    } # failure
    
    if (failure &   sapply(strsplit(articleTitleMod, "\\s+"), length) > 1) {
        
        # remove last word
        
        articleTitle <- sub('\\s[[:alpha:]]+$',"",articleTitleMod)
        # articleTitle <- stri_trans_general(str = articleTitle, id = "Latin-ASCII")
        # articleTitleMod<-monthArticles[which(monthArticles$PAGES=="1--5"),]$TITLE[1]
        
        initpage<-NA
        searchIn<-monthArticles$INDEXFILE[i]
        # searchIn<-monthArticles[which(monthArticles$PAGES=="1--5"),]$INDEXFILE[1]
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",searchIn,"'") , intern=T )
                 , warning=function(w){
                 }
        )
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        }
        
    } # failure
    
    if (failure) {
        #
        #   tre-agrep
        #
        
        symbols <- c(
            acute = "áéíóúÁÉÍÓÚýÝ",
            grave = "àèìòùÀÈÌÒÙ",
            circunflex = "âêîôûÂÊÎÔÛ",
            tilde = "ãõÃÕñÑ",
            umlaut = "äëïöüÄËÏÖÜÿ",
            cedil = "çÇ"
        )
        #remove special
        # articleTitleMod<-monthArticles[which(monthArticles$PAGES=="1--5"),]$TITLE[1]
        
        articleTitle<-gsub(paste0(unlist(strsplit(toString(paste0(symbols, collapse="") ),"") ),collapse = "|"),"",articleTitleMod)
        
        initpage<-NA
        searchIn<-monthArticles$INDEXFILE[i]
        
        tryCatch(row <- system(paste0("tre-agrep -i -s -2 '", articleTitle,"' '",searchIn,"'") , intern=T )
                 , warning=function(w){
                 }
        )
        
        remove(initpage)
        initpage<- suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        }
    } # failure
    
    
    if (failure) {
        #
        # keywords based
        #
        articleTitle<-monthArticles$KEYWORDS[i]
        articleTitle <- stri_trans_general(str = articleTitle, id = "Latin-ASCII")
        initpage<-NA
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",monthArticles$INDEXFILE[i],"'") , intern=T )
                 , warning=function(w){
                 }
        )
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        } 
    } # failure
    if(failure) {
        #
        # keywords based
        #
        articleTitle<-ifelse(wc==1,monthArticles$KEYWORDS[i],articleTitleMod)
        # articleTitle<-monthArticles$KEYWORDS[i]
        initpage<-NA
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",monthArticles$INDEXFILE[i],"'") , intern=T )
                 , warning=function(w){
                 }
        )
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        }
    } # failure    
    if(failure) {
        #
        # keywords based
        #
        articleTitle<-monthArticles$KEYWORDS[i]
        initpage<-NA
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",monthArticles$INDEXFILE[i],"'") , intern=T )
                 , warning=function(w){
                 }
        )
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        if(length(initpage) ) {
            testInit(initpage,monthArticles,failure)
        }
    } # failure
    if (failure &  sapply(strsplit(monthArticles$KEYWORDS[i], "\\s+"), length) > 1 ) {
        #
        # keywords based
        #
        articleTitle <- monthArticles$KEYWORDS[i]
        articleTitle <- stri_trans_general(str = articleTitle, id = "Latin-ASCII")
        articleTitle <- sub("([[:alpha:]]+).*","\\1",articleTitle)
        initpage<-NA
        tryCatch(row <- system(paste0("grep -i '", articleTitle,"' '",monthArticles$INDEXFILE[i],"'") , intern=T )
                 , warning=function(w){
                     print(paste("5th orig",articleTitleOrig) )
                     print(paste("5th",articleTitle) )
                 }
        )
        remove(initpage) 
        initpage<-suppressWarnings(as.numeric(sub("\\s?([0-9]+).*","\\1",row) ) )
        if(length(initpage) ) {
            initpage <- unique(initpage)
            initpage <- initpage[!is.na(initpage)]
            initpage <- initpage[!initpage>500]
            initpage <- initpage[!initpage<3]
            
            maxLen <- tryCatch(ifelse(length(initpage)>4,4,length(initpage) ) ,error=function(e){1} )
            initpage <- initpage[1:maxLen]
            
            if (length(initpage)>1) {
                print(paste0("5th",initpage, collapse = " "))
                initpage <- paste0(initpage, collapse = ", ")
            } 
            if(length(initpage) ) {
                if(!is.na(initpage)){
                    monthArticles$PAGES[i]<-initpage
                    print(paste("5th success"))
                } else {
                    print(paste("5th NA",articleTitleOrig) )
                    print(paste("5th NA",articleTitle) )
                    error<-error+1
                }
            } else {
                print(paste("5th miss",articleTitleOrig) )
                print(paste("5th miss",articleTitle) )
                error<-error+1
            }
            #
        } else {
            print(paste("5th miss in",articleTitleOrig) )
            print(paste("5th miss in",articleTitle) )
            error<-error+1
        }
    } # failure
    # else {
    #     error <-error+1
    # }
}

error
monthArticles$PAGES
