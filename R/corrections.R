githome<-"/home/fernando/GoogleDrive/gitlab/InveCien/pdfs/"
require(data.table)
artsec<-fread(paste0(githome,"artisec.csv"))

head(artsec)
dim(artsec)
colnames(artsec)
unique(artsec$AUTHOR)

#
# CORRECTION
#

artsec$AUTHOR<-sub("character\\(0\\)","",artsec$AUTHOR)

#
#        correction
#

artsec$AUTHOR<-gsub("c\\(\"\"\"\"","",artsec$AUTHOR)
artsec$AUTHOR<-gsub("\"\"\"\"","",artsec$AUTHOR)
artsec$AUTHOR<-gsub("\\)","",artsec$AUTHOR)
artsec$ABSTRACT<-gsub('["]+','"',artsec$ABSTRACT)
artsec$TITLE<-gsub('["]+','"',artsec$TITLE)

artsec[which(artsec$TITLE==""),]
artsec[which(is.na(artsec$TITLE) ),]

grep("de color",artsec$ABSTRACT, value=T) 



#
#       remove duplicate names
#

for (i in 1:length(artsec$AUTHOR) ){
    if(artsec$AUTHOR[i]!=""){
        aut<-artsec$AUTHOR[i]
        unaut  <- unique(unlist(strsplit(aut,", ") ) )
        newaut <- unlist(lapply( unaut, function(x) sub("\n","",x) ) ) 
        newaut <- paste(unique(newaut ), collapse=", ")
        artsec$AUTHOR[i] <-  newaut
    }
}


artsec$ncharA<- nchar(artsec$AUTHOR) 

# artsec[which(artsec$ncharA==max(artsec$ncharA)),]$AUTHOR
artsec[order(artsec$ncharA, decreasing=T),]$AUTHOR[1:20]
artsec[order(artsec$ncharA, decreasing=T),]$AUTHOR[2]

#
#       remove pseudo-duplicated names
#

for (i in 1:length(artsec$ncharA) ){
    if(artsec$ncharA[i]>0){
        split<-unlist(strsplit(artsec$AUTHOR[i], ", ") )
        toremove<-numeric()
        mgrep<-as.character(NA)
        for (author in split){
            mgrep<-(grep(author, unlist(strsplit(artsec$AUTHOR[i],", " ) ) ,value=T)  )
            mplace<-grep(author, unlist(strsplit(artsec$AUTHOR[i],", " ) )   )
            
            if(length(mgrep)>1){
                toremove<-(mplace[maxM] )
            }
        }
        if(length(toremove)>0){
            cleansplit<-split[-toremove]
            artsec$AUTHOR[i] <- paste(cleansplit,collapse=", ")
        }
    }
}

artsec$AUTHOR <- gsub(","," and", artsec$AUTHOR )

#
# CORRECTIONS
#

require(data.table)
artsec<-fread(paste0(githome,"artisec.csv"))
dim(artsec)
unique(artsec$KEYWORDS)
artsec[which(artsec$KEYWORDS %in% grep("SERIE: LA|SERIE: IN", artsec$KEYWORDS, value=T)),]$KEYWORDS
artsec[which(artsec$KEYWORDS %in% grep("SERIE: LA|SERIE: IN", artsec$KEYWORDS, value=T)),]$KEYWORDS<-"SERIE: MECÁNICA CUÁNTICA"
colnames(artsec)
artsec[which(artsec$KEYWORDS %in% grep("TENDENCIAS EN LA F", artsec$KEYWORDS, value=T)),]$KEYWORDS
artsec[which(artsec$KEYWORDS %in% grep("TENDENCIAS EN LA F", artsec$KEYWORDS, value=T)),]$KEYWORDS<-"FABRICACIÓN DE SEMICONDUCTORES"
