
<!-- README.md is generated from README.Rmd. Please edit that file -->

<img src=figures/logo.png align="right" width="12%" hspace="50">

## linkScraping: Creating a .bib library from journal html pages

The goal of linkScraping is to scrap the journal pages and create a .bib
library.

It uses python to scrap and R to organize data.

### Scripts in order of use

-   py/pyScrap.py : gets links of every issue in a raw format into
    folder sourcelinks

<!-- -->

    $`2020`
    [1] "https://www.investigacionyciencia.es/revistas/investigacion-y-ciencia/el-mundo-ante-la-pandemia-800"  
    [2] "https://www.investigacionyciencia.es/revistas/investigacion-y-ciencia/una-crisis-csmica-798"          
    [3] "https://www.investigacionyciencia.es/revistas/investigacion-y-ciencia/el-nuevo-coronavirus-796"       
    [4] "https://www.investigacionyciencia.es/revistas/investigacion-y-ciencia/por-qu-estamos-solos-792"       
    [5] "https://www.investigacionyciencia.es/revistas/investigacion-y-ciencia/escapar-de-un-agujero-negro-791"
    [6] "https://www.investigacionyciencia.es/revistas/investigacion-y-ciencia/cristales-en-el-tiempo-788"     

-   py/pyScrapDownFields.py : Opens links and downloads article info as
    dictionary and pandas (.csv) into folder `csv`

-   py/pyScrapDownSecondInfo.py: Opens links and downloads secondary
    section info as dictionary and pandas (.csv) into folder `csv`

``` r
dfAll<-data.table::fread("csv/dfAll2.csv")
knitr::kable(head(dfAll[,1:8]))
```

|  V1 | BIBTEXKEY       | CATEGORY | year | volume    | number | pages | title                                                          |
|----:|:----------------|:---------|-----:|:----------|-------:|:------|:---------------------------------------------------------------|
|   1 | Llorens\_1980   | article  | 1980 | DICIEMBRE |     51 | 1–5   | Física y tecnología del reactor de fusión                      |
|   2 | Margon\_1980    | article  | 1980 | DICIEMBRE |     51 | 1–5   | El curioso espectro de SS 433                                  |
|   3 | Milstein\_1980  | article  | 1980 | DICIEMBRE |     51 | 1–5   | Anticuerpos monoclonales                                       |
|   4 | Nassau\_1980    | article  | 1980 | DICIEMBRE |     51 | 1–5   | Las causas del color                                           |
|   5 | Cook\_1980      | article  | 1980 | DICIEMBRE |     51 | 1–5   | Los Apalaches meridionales y el crecimiento de los continentes |
|   6 | Pogossian\_1980 | article  | 1980 | DICIEMBRE |     51 | 1–5   | Tomografía por emisión de positrones                           |

-   py/pyScrapDownPdf\_safe.py: Opens links and downloads .pdfs (needs
    account)

-   R/csvTobib : After having a .csv transforms to .bib or .xlsx

### Updating data base

-   R/addMonth.R: reads .csv files and merge them in an object
    `artsecMonthsYear` == `monthArticles`

-   R/addPagesMonth.R: reads pdfs (needs account to download them) and
    add pages `monthArticles$PAGES`

-   R/mergeMonth.R: merge downloaded month with previous data, and
    create .bib for current month

-   R/updateIndex.R: rewrite decade .csv, xlsx (pdfs are organized in
    folders by year and decade with indices in .csv and .xlsx files)
    indices; write issue indices .csv, .xlsx; rewrite whole collection
    .csv and .xlsx
